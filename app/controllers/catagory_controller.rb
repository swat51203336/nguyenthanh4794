class CatagoryController < ApplicationController
  def Online
  	@Gameonlinefilter = Topic.Game("Game Online")
    @online = Topic.where("cate_type = ?", "Game Online").paginate(:page => params[:page],:per_page => 2,:order => "created_at DESC")
  end

  def Offline
  	@Gameofflinefilter = Topic.Game("Game Offline")
    @offline = Topic.where("cate_type = ?", "Game Offline").paginate(:page => params[:page],:per_page => 2,:order => "created_at DESC")   
  end

  def Mobile
  	@Gamemobilefilter = Topic.Game("Game Mobile")
    @mobile = Topic.where("cate_type = ?", "Game Mobile").paginate(:page => params[:page],:per_page => 2,:order => "created_at DESC")
  end
  def blank
    render :layout => false
  end
end
