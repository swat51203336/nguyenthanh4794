class TopicsController < ApplicationController
  before_filter :load_forum

  # GET /topics
  # GET /topics.json
  def index
    @topics = Topic.all

    respond_to do |format|
      format.html
      format.xml { render :xml => @topic }
    end
  end

  # GET /topics/1
  # GET /topics/1.json
  def show
    @topic = Topic.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml { render :xml => @topic }
    end
  end
  def haha
  end
  # GET /topics/new
  def new
    @topic = Topic.new
    @comment = @topic.comments.build

    render :layout => false
  end

  def upvote
    @topic = Topic.find(params[:id])
    @topic.votes.create
    redirect_to forum_topic_path(:forum_id => @forum, :id => @topic)
  end

  def commentreply
    @topic = Topic.find(params[:id])
    @comment = @topic.comments.build
    @time = Time.new

    render :layout => false
  end

  def save_commentreply
    params.permit!
    if Topic.exists?(params[:id])
      @topic = Topic.find(params[:id])
      @comment = @topic.comments.build(params[:comment])
      @comment.user_name = current_user.name
      @comment.user_id = current_user.id
      @comment.cmt_date = @time.inspect
    else
      redirect_to(forums_path, :notice =>"Please specify a valid Forum")
    end
    @blank = catagory_blank_path
     respond_to do |format|
      if @comment.save
        format.html { redirect_to @blank, :notice => 'Your reply was posted'}
      else
        format.html { render :action => "new" }
      end
    end
  end

  # GET /topics/1/edit
  def edit
    @topic = Topic.find(params[:id])
  end

  # POST /topics
  # POST /topics.json
  def create
    params.permit!
    @topic = Topic.new(topic_params)
    @time = Time.new
    @comment = @topic.comments.build(params[:comment])
    @comment.user_name = current_user.name
    @comment.cmt_date = @time.inspect
    @topic.topic_date = @time.inspect
    @topic.user_name = current_user.name
    @topic.forum = @forum

    respond_to do |format|
      if @topic.save
        format.html { redirect_to forum_topic_path(:forum_id => @forum, :id => @topic), notice: 'Topic was successfully created.' }
        format.json { render inline: "location.reload();" }
      else
        format.html { render action: 'new' }
        format.json { render json: @topic.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /topics/1
  # PATCH/PUT /topics/1.json
  def update
    @topic = Topic.find(params[:id])
    respond_to do |format|
      if @topic.update(topic_params)
        format.html { redirect_to forum_topic_path(:forum_id => @forum, :id => @topic), notice: 'Topic was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @topic.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /topics/1
  # DELETE /topics/1.json
  def destroy
    @topic = Topic.find(params[:id])
    @topic.destroy

    @topic.destroy
    respond_to do |format|
      format.html { redirect_to forums_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_topic
      @topic = Topic.find(params[:id])
    end

    def load_forum
        if Forum.exists?(params[:forum_id])
          @forum = Forum.find(params[:forum_id]);
        end
        unless @forum
          redirect_to(forums_path, :notice =>"Please specify a valid Forum")
        end     
    end
    # Never trust parameters from the scary internet, only allow the white list through.
    def topic_params
      params.require(:topic).permit(:title, :forum_id, :comment_id, :cate_type, :description, :img_head, :img_body, :img_footer, :link)
    end
end
