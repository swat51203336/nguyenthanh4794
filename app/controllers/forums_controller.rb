class ForumsController < ApplicationController
  before_action :set_forum, only: [:show, :edit, :update, :destroy]

  # GET /forums
  # GET /forums.json
  def index
    @forums = Forum.all
    render :layout => false
  end

  # GET /forums/1
  # GET /forums/1.json
  def show
    @online = Topic.where("cate_type = ?", "Game Online").paginate(:page => params[:page],:per_page => 2,:order => "created_at DESC")
    @offline = Topic.where("cate_type = ?", "Game Offline").paginate(:page => params[:page],:per_page => 2,:order => "created_at DESC")
    @mobile = Topic.where("cate_type = ?", "Game Mobile").paginate(:page => params[:page],:per_page => 2,:order => "created_at DESC")
    @newfeed = Topic.where("cate_type = ?", "New Feed").paginate(:page => params[:page],:per_page => 2,:order => "created_at DESC")
  end

  # GET /forums/new
  def new
    @forum = Forum.new
    render :layout => false
  end

  # GET /forums/1/edit
  def edit
  end

  # POST /forums
  # POST /forums.json
  def create
    @forum = Forum.new(forum_params)
    @blank = catagory_blank_path
    respond_to do |format|
      if @forum.save
        format.html { redirect_to @blank, notice: 'Forum was successfully created.' }
        format.json { render action: 'show', status: :created, location: @forum }
      else
        format.html { render action: 'new' }
        format.json { render json: @forum.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /forums/1
  # PATCH/PUT /forums/1.json
  def update
    respond_to do |format|
      if @forum.update(forum_params)
        format.html { redirect_to @forum, notice: 'Forum was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @forum.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /forums/1
  # DELETE /forums/1.json
  def destroy
    @forum.destroy
    respond_to do |format|
      format.html { redirect_to forums_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_forum
      @forum = Forum.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def forum_params
      params.require(:forum).permit(:title)
    end
end
