class Topic < ActiveRecord::Base
	belongs_to :user
	belongs_to :forum
	has_many :comments, :dependent => :destroy
	has_many :votes, :dependent => :destroy
	validates_presence_of :title
	validates_length_of :title, :maximum=>50

	def self.Game(type)
		Topic.where("cate_type = ?", type)
	end
end	
