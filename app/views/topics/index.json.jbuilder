json.array!(@topics) do |topic|
  json.extract! topic, :id, :title, :forum_id, :comment_id
  json.url topic_url(topic, format: :json)
end
