require 'test_helper'

class CatagoryControllerTest < ActionController::TestCase
  test "should get Online" do
    get :Online
    assert_response :success
  end

  test "should get Offline" do
    get :Offline
    assert_response :success
  end

  test "should get Mobile" do
    get :Mobile
    assert_response :success
  end

  test "should get Newfeed" do
    get :Newfeed
    assert_response :success
  end

end
