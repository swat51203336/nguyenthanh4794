require 'test_helper'

class CategorosControllerTest < ActionController::TestCase
  setup do
    @categoro = categoros(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:categoros)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create categoro" do
    assert_difference('Categoro.count') do
      post :create, categoro: { forum_id: @categoro.forum_id, name: @categoro.name }
    end

    assert_redirected_to categoro_path(assigns(:categoro))
  end

  test "should show categoro" do
    get :show, id: @categoro
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @categoro
    assert_response :success
  end

  test "should update categoro" do
    patch :update, id: @categoro, categoro: { forum_id: @categoro.forum_id, name: @categoro.name }
    assert_redirected_to categoro_path(assigns(:categoro))
  end

  test "should destroy categoro" do
    assert_difference('Categoro.count', -1) do
      delete :destroy, id: @categoro
    end

    assert_redirected_to categoros_path
  end
end
