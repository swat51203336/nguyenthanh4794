class CreateCategoros < ActiveRecord::Migration
  def change
    create_table :categoros do |t|
      t.string :name
      t.integer :forum_id

      t.timestamps
    end
  end
end
