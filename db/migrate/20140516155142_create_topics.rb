class CreateTopics < ActiveRecord::Migration
  def self.up
    create_table :topics do |t|
      t.string :title
      t.integer :forum_id
      t.integer :comment_id

      t.timestamps
    end
    add_index :topics, :forum_id
  end

  def self.down
  	drop_table :topics  	
  end
end
